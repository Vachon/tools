#!/bin/bash
#!/usr/bin/env bash

# Software
# Up to Date
apt update && apt upgrade -y

# Tools
apt install yum yum-utils wget openssh-server yum iotop subversion git vim csh tcsh zsh clustershell atop htop quota screen tmux tree mc dia inkscape gimp joe xfig gftp filezilla texlive g++ r-base texlive-full -y

# Create tmp directory
mkdir /tmp/icminstall
cd /tmp/icminstall

# Teamviewer
wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb
apt install ./teamviewer_amd64.deb -y

# ZOOM
wget https://zoom.us/client/latest/zoom_amd64.deb
apt install ./zoom_amd64.deb -y

# Anydesk & OwnCloud
# Repo Anydesk
wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | apt-key add -
echo "deb http://deb.anydesk.com/ all main" > /etc/apt/sources.list.d/anydesk-stable.list

# Repo OwnCloud
echo "deb http://download.opensuse.org/repositories/isv:/ownCloud:/desktop/Linux_Mint_19/ /" > /etc/apt/sources.list.d/isv:ownCloud:desktop.list
wget -nv https://download.opensuse.org/repositories/isv:ownCloud:desktop/Linux_Mint_19/Release.key -O Release.key
apt-key add - < Release.key

# Install
apt update
apt install anydesk -y
apt install owncloud-client -y

# OpenVPN
wget https://openvpn.net/downloads/openvpn-as-bundled-clients-latest.deb
apt install ./openvpn-as-bundled-clients-latest.deb -y

# Remove tmp directory
rm -rf /tmp/icminstall

# Up to Date
apt update && apt upgrade -y
